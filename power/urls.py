from django.conf.urls import url
from .views import index, exponent_zero, calculate_power
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^(?P<base>\d+)/$', exponent_zero, name='exponent_zero'),
    url(r'^(?P<base>\d+)/(?P<exponent>\d+)/$', calculate_power, name='calculate_power'),
]
