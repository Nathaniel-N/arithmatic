from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
import requests

# Create your views here.
def index(request):
    return HttpResponseRedirect('/power/0/0')

def exponent_zero(request, base):
	return HttpResponseRedirect('/power/'+base+'/0')

def calculate_power(request, base, exponent):
    value = int(base) ** int(exponent)
    data = {'base': base, 'exponent': exponent, 'value': value}
    return JsonResponse(data)